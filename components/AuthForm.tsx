import { useState, FunctionComponent } from 'react';

import { AuthCredential } from '../types/index';

type PageProps = {
  submitForm: (loginData: AuthCredential) => void;
};

const AuthForm: FunctionComponent<PageProps> = ({ submitForm }) => {
  const [loginData, setLoginData] = useState({
    email: '',
    password: '',
  });

  const handleChange = (e) => {
    setLoginData({
      ...loginData,
      [e.target.name]: e.target.value,
    });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    if (loginData.email && loginData.password) {
      submitForm(loginData);
    }
  };
  return (
    <div style={{ padding: 20, display: 'flex', flexDirection: 'column' }}>
      <input
        type='text'
        name='email'
        value={loginData.email}
        onChange={handleChange}
        style={{ marginBottom: 10 }}
      />
      <input
        type='text'
        name='password'
        value={loginData.password}
        onChange={handleChange}
        style={{ marginBottom: 10 }}
      />
      <button onClick={handleSubmit}>Login</button>
    </div>
  );
};
export default AuthForm;
