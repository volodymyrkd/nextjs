import * as React from 'react';
import { PageProps } from '../types';

const Footer: React.FunctionComponent<PageProps> = ({ regions }) => {
  return (
    <footer>
      <nav>
        {regions.map((page, idx) => (
          <React.Fragment key={idx}>
            <span>{page.name}</span>|{' '}
          </React.Fragment>
        ))}
      </nav>
    </footer>
  );
};
export default Footer;
