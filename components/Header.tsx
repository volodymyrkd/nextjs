import * as React from 'react';
import Link from 'next/link';

import { PageProps } from '../types';
import { useSelector, useDispatch } from 'react-redux';

import { authSelectors, authOperations } from '../store/auth';
import { IRootState } from '../store';

const Header: React.FunctionComponent<PageProps> = ({ pages }) => {
  const dispatch = useDispatch();
  const isAuth = useSelector((state: IRootState) =>
    authSelectors.isAuth(state.auth)
  );

  const handleLogout = () => {
    dispatch(authOperations.logout());
  };

  return (
    <header>
      <nav>
        <Link href='/login'>
          <a>Login</a>
        </Link>{' '}
        <Link href='/private'>
          <a>Private page</a>
        </Link>{' '}
      </nav>
      <nav>
        {pages.map((page, idx) => (
          <React.Fragment key={idx}>
            <Link href='/post/[pid]' as={`/post/${page.title}`}>
              <a>{page.title}</a>
            </Link>{' '}
            |{' '}
          </React.Fragment>
        ))}
      </nav>
      {isAuth && <button onClick={handleLogout}>Logout</button>}
    </header>
  );
};
export default Header;
