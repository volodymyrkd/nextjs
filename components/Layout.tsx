import * as React from 'react';
import Head from 'next/head';
import Header from './Header';
import Footer from './Footer';

import { PageProps } from '../types';

type LayoutProps = {
  title?: string;
} & PageProps;

const layoutStyle = {
  margin: 20,
  padding: 20,
  border: '1px solid #DDD',
};

const Layout: React.FunctionComponent<LayoutProps> = ({
  children,
  pages,
  regions,
}) => {
  return (
    <div style={layoutStyle}>
      <Head>
        <title>default title</title>
        <meta charSet='utf-8' />
        <meta name='viewport' content='initial-scale=1.0, width=device-width' />
      </Head>
      <Header pages={pages} />
      {children}
      <Footer regions={regions} />
    </div>
  );
};
export default Layout;
