import { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';

import Router from 'next/router';

import { IRootState } from '../store';
import { authSelectors } from '../store/auth';

const useGuest = () => {
  const [mount, setMount] = useState(true);
  const isAuth = useSelector((state: IRootState) =>
    authSelectors.isAuth(state.auth)
  );

  useEffect(() => {
    if (isAuth) {
      Router.push('/private');
    }
    setMount(false);
  }, [isAuth, setMount]);

  return mount;
};

export { useGuest };
