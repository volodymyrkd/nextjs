import Router from 'next/router';

import cookies from 'js-cookie';

import { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';

import { authOperations, authSelectors } from '../store/auth';
import { IRootState, store } from '../store';

const usePrivate = () => {
  const [mount, setMount] = useState(true);

  const isAuth = useSelector((state: IRootState) =>
    authSelectors.isAuth(state.auth)
  );
  const isLoading = useSelector((state: IRootState) =>
    authSelectors.isLoading(state.auth)
  );
  //   const token = cookies.get('token');

  useEffect(() => {
    // if (!token) {
    //   store.dispatch(authOperations.logout());
    // }
    if (!isAuth && !isLoading) {
      Router.push('/login');
    }
    setMount(false);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isAuth, isLoading, setMount]);

  return mount;
};

export { usePrivate };
