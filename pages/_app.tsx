import { useEffect, useState } from 'react';
import axios from 'axios';
import { Provider } from 'react-redux';

import { store } from '../store';

import Layout from '../components/Layout';

function MyApp({ Component, pageProps }) {
  const [pages, setPages] = useState([]);
  const [regions, setRegions] = useState([]);

  // we can keep here some state
  // use componentDidCatch
  // add aditional props to next components
  // add some css

  // bad to use getInitialProps
  useEffect(() => {
    const send = async () => {
      const res = await axios.get(
        'http://web-api.mcm.thedevelopmentserver.com/pages'
      );
      setPages(res.data.data);
    };
    send();
  }, [setPages]);

  useEffect(() => {
    const send = async () => {
      const res = await axios.get(
        'http://web-api.mcm.thedevelopmentserver.com/regions'
      );
      setRegions(res.data.data);
    };
    send();
  }, [setRegions]);

  return (
    <Provider store={store}>
      <Layout pages={pages} regions={regions}>
        <Component {...pageProps} />
      </Layout>
    </Provider>
  );
}

export default MyApp;
