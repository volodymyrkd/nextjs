import React, { Component } from 'react';
import Document, {
  Head,
  Main,
  NextScript,
  DocumentInitialProps,
} from 'next/document';

// we cant add here logic
// we cant use some events like onClick...
// ctx.req need to be undefined
export default class MyDocument extends Document<DocumentInitialProps> {
  static async getInitialProps(ctx) {
    const originalRenderPage = ctx.renderPage;
    //here we can add our styles module
    //
    //
    ctx.renderPage = () =>
      originalRenderPage({
        // useful for wrapping the whole react tree
        enhanceApp: (App: Component) => App,
        // useful for wrapping in a per-page basis
        enhanceComponent: (Component: Component) => Component,
      });

    // Run the parent `getInitialProps`, it now includes the custom `renderPage`
    const initialProps = await Document.getInitialProps(ctx);

    return initialProps;
  }

  render() {
    return (
      <html>
        <Head>
          <meta property='og:title' content='My page title' key='title' />
          <meta
            name='viewport'
            content='width=device-width,initial-scale=1,viewport-fit=cover'
          />
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </html>
    );
  }
}
