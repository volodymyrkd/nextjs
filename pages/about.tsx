import React from 'react';
import Head from 'next/head';
import { PageProps } from '../types';

const About: React.FunctionComponent<PageProps> = () => {
  return (
    <>
      <Head>
        <title>My about page</title>
      </Head>
      <h1>This is About page ✌</h1>
    </>
  );
};
export default About;
