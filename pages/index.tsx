import React from 'react';
import { PageProps } from '../types';

const Home: React.FunctionComponent<PageProps> = () => {
  return <h1>Hello Next.js 👋</h1>;
};
export default Home;
