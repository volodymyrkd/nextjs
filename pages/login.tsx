import React, { useCallback } from 'react';
import { useDispatch } from 'react-redux';
import AuthForm from '../components/AuthForm';

import { AuthCredential } from '../types/index';

import { authOperations } from '../store/auth';

import { useGuest } from '../hooks/useGuest';

const Login: React.FunctionComponent = () => {
  const authMount = useGuest();

  const dispatch = useDispatch();
  const submitForm = useCallback(
    (credential: AuthCredential) =>
      dispatch(authOperations.loginRequest(credential)),
    [dispatch]
  );

  if (authMount) {
    return null;
  }

  return <AuthForm submitForm={submitForm} />;
};
export default Login;
