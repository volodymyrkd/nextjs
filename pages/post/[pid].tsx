import React from 'react';
import { useRouter } from 'next/router';
import Head from 'next/head';

import { PageProps } from '../../types';

const Post: React.FunctionComponent<PageProps> = () => {
  const router = useRouter();
  const { pid } = router.query;

  return (
    <>
      <Head>
        <title>{pid || 'local'}</title>
      </Head>
      <h1>This is {pid} page with dynamic route ✌</h1>
    </>
  );
};
export default Post;
