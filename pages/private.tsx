import React from 'react';
import { useSelector } from 'react-redux';

import { usePrivate } from '../hooks/usePrivate';
import { authSelectors } from '../store/auth';

import { IRootState } from '../store';

const Home: React.FunctionComponent = () => {
  const authMount = usePrivate();

  const userData = useSelector((state: IRootState) =>
    authSelectors.getUser(state.auth)
  );
  console.log(userData);
  if (authMount) {
    return null;
  }
  return <h1>This is a private page</h1>;
};
export default Home;
