import types from './types';

import { AuthCredential } from '../../types';

const loginRequest = (payload: AuthCredential) => ({
  type: types.LOGIN_REQUEST,
  payload,
});

const loginSuccess = () => ({
  type: types.LOGIN_SUCCESS,
});

const loginFailed = () => ({
  type: types.LOGIN_FAILED,
});
const getMe = () => ({
  type: types.ME_REQUEST,
});

const setMe = (user) => ({
  payload: user,
  type: types.ME_SUCCESS,
});
const logout = () => ({
  type: types.LOGOUT_REQUEST,
});

const logoutSuccess = () => ({
  type: types.LOGOUT_SUCCESS,
});

export default {
  loginFailed,
  loginSuccess,
  logout,
  logoutSuccess,
  loginRequest,
  getMe,
  setMe,
};
