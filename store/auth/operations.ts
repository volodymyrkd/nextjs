import { call, put, takeEvery } from '@redux-saga/core/effects';
import cookie from 'js-cookie';
import Router from 'next/router';

import axios from 'axios';

import actions from './actions';

import types from './types';

function* login(token: string) {
  yield call(cookie.set, 'token', token);
  console.log(token);
  yield put(actions.loginSuccess());
  yield put(actions.getMe());
}

function* emailLogin({ payload }: any) {
  try {
    const res = yield call(
      axios.post,
      'http://tenant-api.mcm.thedevelopmentserver.com/auth/login',
      payload
    );

    yield call(login, res.data.token);
  } catch (e) {
    yield put(actions.loginFailed());
  }
}
function* doLogout() {
  cookie.remove('token');
  window.localStorage.setItem('logout', String(Date.now()));
  Router.push('/login');
  yield put(actions.logoutSuccess());
}

function* fetchMe() {
  const token = yield call(cookie.get, 'token');
  const res = yield call(
    axios.get,
    'http://tenant-api.mcm.thedevelopmentserver.com/auth/me',
    {
      headers: {
        Authorization: `Bearer ${token}`,
        'Content-Type': 'application/x-www-form-urlencoded',
      },
    }
  );
  yield put(actions.setMe(res.data.data));
}

function* rootAuthSagas() {
  yield takeEvery(types.LOGIN_REQUEST, emailLogin);
  yield takeEvery(types.ME_REQUEST, fetchMe);
  yield takeEvery(types.LOGOUT_REQUEST, doLogout);
}

export default {
  rootAuthSagas,
  ...actions,
};
