import { AuthReducer } from '../index';

import types from './types';

const initialState: AuthReducer = {
  isAuth: false,
  isLoading: false,
  user: null,
};

const authReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case types.LOGIN_REQUEST:
    case types.ME_REQUEST:
      return {
        ...state,
        isLoading: true,
      };
    case types.LOGIN_FAILED:
      return {
        ...state,
        isAuth: false,
        isLoading: false,
      };
    case types.LOGIN_SUCCESS:
      return {
        ...state,
        isAuth: true,
        isLoading: false,
      };
    case types.ME_SUCCESS:
      return {
        ...state,
        isAuth: true,
        isLoading: false,
        user: payload,
      };
    case types.LOGOUT_SUCCESS:
      return { ...initialState, isAuth: false, isLoading: false };
    default:
      return state;
  }
};
export default authReducer;
