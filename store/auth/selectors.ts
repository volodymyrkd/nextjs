import { AuthReducer } from '../index';

const isAuth = (auth: AuthReducer) => auth.isAuth;
const isLoading = (auth: AuthReducer) => auth.isLoading;
const getUser = (auth: AuthReducer) => auth.user;

export default {
  getUser,
  isAuth,
  isLoading,
};
