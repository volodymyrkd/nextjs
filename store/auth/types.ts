const LOGOUT_REQUEST = '@@auth/LOGOUT_REQUEST';
const LOGOUT_SUCCESS = '@@auth/LOGOUT_SUCCESS';
const LOGIN_FAILED = '@@auth/LOGIN_FAILED';
const LOGIN_SUCCESS = '@@auth/LOGIN_SUCCESS';
const LOGIN_REQUEST = '@@auth/LOGIN_REQUEST';
const ME_REQUEST = '@@auth/ME_REQUEST';
const ME_SUCCESS = '@@auth/ME_SUCCESS';

export default {
  ME_REQUEST,
  ME_SUCCESS,
  LOGIN_REQUEST,
  LOGIN_FAILED,
  LOGIN_SUCCESS,
  LOGOUT_REQUEST,
  LOGOUT_SUCCESS,
};
