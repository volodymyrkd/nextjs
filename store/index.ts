import { applyMiddleware, combineReducers, createStore, Store } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';

import createSagaMiddleware from 'redux-saga';
import { all } from 'redux-saga/effects';

import authReducer, { authOperations } from './auth';

import { UserCredential } from '../types/user';

export type AuthReducer = {
  isAuth: boolean;
  isLoading: boolean;
  user: UserCredential | null;
};

const rootReducer = combineReducers({
  auth: authReducer,
});

export interface IRootState {
  auth: AuthReducer;
}

export function* rootSaga() {
  yield all([authOperations.rootAuthSagas()]);
}

// create the composing function for our middleware
const composeEnhancers = composeWithDevTools({});
// create the redux-saga middleware
const sagaMiddleware = createSagaMiddleware();
// We'll create our store with the combined reducers/sagas, and the initial Redux state that
// we'll be passing from our entry point.

export const store: Store<IRootState> = createStore(
  rootReducer,
  composeEnhancers(applyMiddleware(sagaMiddleware))
);

sagaMiddleware.run(rootSaga);
