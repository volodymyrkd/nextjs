export type PageProps = {
  pages?: any;
  regions?: any;
};

export type AuthCredential = {
  email: string;
  password: string;
};
