export type UserCredential = {
  id: string;
  first_name: string;
  last_name: string;
  email: string | null;
  email_verified_at: string | null;
  click_pay_url: string;
};
